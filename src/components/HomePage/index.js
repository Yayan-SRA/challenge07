import Navbar from '../Navbar';
import Carousel from '../Carousel';
import OurService from '../OurService';
import WhyUs from '../WhyUs';
import Testimoni from '../Testimoni';
import BonusPage from '../BonusPage';
import Faq from '../Faq';
import Footer from '../Footer';


function HomePage() {
    return (
        <div>
            <Navbar />
            <Carousel />
            <OurService />
            <WhyUs />
            <Testimoni />
            <BonusPage />
            <Faq />
            <Footer />
        </div>
    );
}

export default HomePage;
