import * as React from 'react';
import ListCars from '../ListCars';
function refreshPage() {
    window.location.reload();
}

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tipedriver: '', jumlahpenumpang: '', tanggal: '', waktu: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Hapus = this.Hapus.bind(this);
        this.data = this.data.bind(this)
    }

    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    handleSubmit(event) {
        event.preventDefault();
        // alert('A name was submitted: ' + this.state.value);
        // event.preventDefault();
        console.log(this.state.tipedriver);
        console.log(this.state.jumlahpenumpang);
        console.log(this.state.tanggal);
        console.log(this.state.waktu);
        // const jumlahpenumpang = this.state.jumlahpenumpang;

    }
    Hapus() {
        return 2
    }


    data(event) {
        event.preventDefault();
        const jumlahpenumpang = this.state.jumlahpenumpang
        console.log(jumlahpenumpang);
        const data1 = [this.state.jumlahpenumpang, this.state.tipedriver, this.state.tanggal, this.state.waktu]
        return data1
    }
    render() {
        return (
            <section className="form__section">
                <form onSubmit={this.handleSubmit} className="content ct__form">
                    {/* <div className=""> */}
                    <div className="row row__button">
                        <div className="col-xl-10">
                            <div className="row">
                                <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                                    <label for="driverType" className="form-label">Tipe Driver</label>
                                    <select className="form-select py-3" id="inputGroupSelect01" value={this.state.value} onChange={this.handleChange} name='tipedriver' >
                                        <option hidden>Pilih Tipe Driver</option>
                                        <option value="Dengan Supir">Dengan Sopir</option>
                                        <option value="Lepas Kunci">Tanpa Sopir (Lepas Kunci)</option>
                                    </select>
                                </div>
                                <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                                    <label for="form_date" className="form-label">Tanggal</label>
                                    <input type="date" name='tanggal' value={this.state.tanggal} onChange={this.handleChange} className="form-control py-3" id="tanggal" placeholder="Pilih Tanggal"></input>
                                </div>
                                <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                                    <label for="form_time" className="form-label">Waktu Jemput/Ambil</label>
                                    <input type="time" name='waktu' value={this.state.waktu} onChange={this.handleChange} className="form-control py-3" id="waktu" placeholder="Pilih Waktu" ></input>
                                </div>
                                <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                                    <label for="jumlah" className="form-label">Jumlah Penumpang (optional)</label>
                                    <input type="number" name='jumlahpenumpang' value={this.state.jumlahpenumpang} onChange={this.handleChange} className="form-control py-3" placeholder="Jumlah Penumpang" aria-label="Username" aria-describedby="basic-addon1" />
                                </div>
                            </div>
                        </div>
                        <div className="mt-4 col-12 col-md-12 col-lg-12 col-xl-2 d-flex justify-content-start">
                            <button className="btn btn-success m-1" id="load-btn" name='submit' value="submit">Cari</button>
                            <button id="clear-btn" onClick={refreshPage} className="btn btn-danger m-1">Clear</button>
                        </div>
                    </div>
                </form>
                {/* </div> */}
                <ListCars submit={this.handleSubmit} data={this.data} jumlahpenumpang={this.state.jumlahpenumpang} tipedriver={this.state.tipedriver} tanggal={this.state.tanggal} waktu={this.state.waktu} />
            </section>
        );
    }
}

export default Search;
