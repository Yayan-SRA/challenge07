
// import './App.css';

function Footer() {
    return (
        <footer className="bg-dark text-center text-lg-start text-white">
            <div className="container py-3">
                {/* <!-- Grid container --> */}
                <div className="container p-4">
                    {/* <!--Grid row--> */}
                    <div className="row mt-4">
                        {/* <!--Grid column--> */}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <ul className="list-unstyled mb-0">
                                <li>
                                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                                </li>
                                <li>
                                    <p>binarcarrental@gmail.com</p>
                                </li>
                                <li>
                                    <p>081-233-334-808</p>
                                </li>
                            </ul>
                        </div>
                        {/* <!--Grid column--> */}

                        {/* <!--Grid column--> */}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!" className="text-white">Our Services</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white">Why Us</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white">Testimonial</a>
                                </li>
                                <li>
                                    <a href="#!" className="text-white">FAQ</a>
                                </li>
                            </ul>
                        </div>
                        {/* <!--Grid column-->

                        <!--Grid column--> */}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <p>Connect With Us</p>
                            <ul className="list-unstyled">
                                <li className="py-1">

                                </li>
                            </ul>
                        </div>
                        {/* <!--Grid column--> */}

                        {/* <!--Grid column--> */}
                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <p>Copyright Binar 2022</p>
                        </div>
                        {/* <!--Grid column--> */}
                    </div>
                    {/* <!--Grid row--> */}
                </div>
                {/* <!-- Grid container --> */}
            </div>
        </footer >
    );
}

export default Footer;
