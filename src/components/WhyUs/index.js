

function WhyUs() {
    return (
        <section id="whyus">
            <div className="container">
                <h2>Why Us?</h2>
                <p>Mengapa harus pilih Binar Car Rental?</p>
                <div className="row">
                    {/*<!-- item 1 -->*/}
                    <div className="col-md-6 col-xl-3">
                        <div className="card bg-c-blue order-card">
                            <div className="card-block">
                                <span className="__wuIcon bg-warning"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                <h6 className="fw-bold mt-3">Mobil Lengkap</h6>
                                <p className="m-b-0 text-start">
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                                    terawat
                                </p>
                            </div>
                        </div>
                    </div>
                    {/*<!-- item2 -->*/}
                    <div className="col-md-6 col-xl-3">
                        <div className="card bg-c-green order-card">
                            <div className="card-block">
                                <span className="__wuIcon bg-danger"><i className="fa fa-tag" aria-hidden="true"></i></span>
                                <h6 className="fw-bold mt-3">Mobil Lengkap</h6>
                                <p className="m-b-0 text-start">
                                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                                    rental mobil lain
                                </p>
                            </div>
                        </div>
                    </div>
                    {/*<!-- item3 -->*/}
                    <div className="col-md-6 col-xl-3">
                        <div className="card bg-c-yellow order-card">
                            <div className="card-block">
                                <span className="__wuIcon bg-primary"><i className="fa fa-clock-o" aria-hidden="true"></i></span>
                                <h6 className="fw-bold mt-3">Layanan 24 Jam</h6>
                                <p className="m-b-0 text-start">
                                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                                    tersedia di akhir minggu
                                </p>
                            </div>
                        </div>
                    </div>
                    {/*<!-- item4 -->*/}
                    <div className="col-md-6 col-xl-3">
                        <div className="card bg-c-pink order-card">
                            <div className="card-block">
                                <span className="__wuIcon bg-c-green"><i className="fa fa-trophy" aria-hidden="true"></i></span>
                                <h6 className="fw-bold mt-3">Sopir Profesional</h6>
                                <p className="m-b-0 text-start">
                                    Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                                    tepat waktu
                                </p>
                            </div>
                        </div>
                    </div>
                    {/*<!-- end item -->*/}
                </div>
            </div>
        </section>
    );
}

export default WhyUs;
