import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListCars } from "../../actions/carsAction";
import './ListModule.css';


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;

}
function bagus(coba) {
    const isPositive = getRandomInt(0, 1) === 1;
    const timeAt = new Date();
    const mutator = getRandomInt(1000000, 100000000);
    coba = new Date(timeAt.getTime() + (isPositive ? mutator : -1 * mutator))
    return coba
}

function ListCars({ submit, data, jumlahpenumpang, tipedriver, tanggal, waktu }) {
    const { listCarsResult, listCarsLoading, listCarsError } = useSelector((state) => state.carReducer);
    const dispatch = useDispatch();
    const jumlah = jumlahpenumpang
    let d = (tanggal + "T" + waktu);
    console.log(d);
    let formdate = Date.parse(d);

    console.log(Date.parse(tanggal));
    console.log(typeof (formdate));

    useEffect(() => {
        // get action list cars
        console.log("1. use effect component did mount")
        dispatch(getListCars());
    }, [dispatch])


    return (
        <div className="container">
            <div className="row">
                {/* <h3 className="text-center mt-1 mb-4">List Cars</h3> */}
                {listCarsResult ? (
                    listCarsResult.filter((car) => ((car.capacity >= jumlah) && ((Date.parse((bagus(car.availableAt))) > formdate)))).map((car) => {
                        // let id = car.id
                        return (
                            <div className="col-12 col-sm-6 col-md-4 col-xl-3 d-flex">
                                <div className="card mt-3">
                                    <div className="card-img p-3">
                                        <img src={car.image} className="card-img-top rounded" alt={car.manufacture} weight="100%" height="160px" />
                                    </div>
                                    <div className="card-body">
                                        <h5 className="nama-mobil"><b>{car.manufacture} {car.model}</b></h5>
                                        <p className="harga-mobil"><b>Rp. {car.rentPerDay} / hari</b></p>
                                        <p className="desc">{car.description}</p>
                                        <div className="tentang mt-1"><i className="mt-1 me-2 icon-mobil fa-solid bi-people">  {car.capacity} People</i></div>
                                        <div className="tentang mt-1"><i className="mt-1 me-2 icon-mobil fa-solid bi-gear">  {car.transmission}</i></div>
                                        <div className="tentang mt-1 mb-2"><i className="mt-1 me-2 icon-mobil fa-solid bi-calendar">  {car.year}</i></div>
                                        <a href="#fa" className="btn btn-success d-flex justify-content-center tombol-mobil">Pilih Mobil</a>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                ) : listCarsLoading ? (
                    <p>Loading . . . </p>
                ) : (
                    <p>{listCarsError ? listCarsError : "Data Kosong"}</p>
                )
                }
            </div>
        </div>
    );
}

export default ListCars;
