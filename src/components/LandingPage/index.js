import * as React from 'react';
import { useState, useEffect } from "react";
import Footer from '../Footer';
import Navbar from '../Navbar';
import Carousel from '../Carousel';
import Search from '../Search';

function LandingPage() {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const token = localStorage.getItem("token");
    // 

    useEffect(() => {
        setIsLoggedIn(!!token);
    }, [token]);


    return (
        <div>
            <Navbar />
            <Carousel />

            {!isLoggedIn ? (
                <p>Data Kosong</p>
            ) : (
                <Search />
            )}
            <Footer />
        </div>
    );
}

export default LandingPage;
