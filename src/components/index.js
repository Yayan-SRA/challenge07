import ListCars from "./ListCars";
import SignIn from "./SignIn";
import Protected from "./Protected";
import HomePage from "./HomePage";
import LandingPage from "./LandingPage";
import Carousel from "./Carousel";
import OurService from "./OurService";
import WhyUs from "./WhyUs";
import Testimoni from "./Testimoni";
import BonusPage from "./BonusPage";
import Faq from "./Faq";
import Search from "./Search";



export { ListCars, SignIn, Protected, HomePage, LandingPage, Carousel, OurService, WhyUs, Testimoni, BonusPage, Faq, Search }